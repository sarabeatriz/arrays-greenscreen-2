#ifndef CLICKABLELABEL_H
#define CLICKABLELABEL_H

#include <QLabel>
#include <QMouseEvent>
#include <QDebug>

/// A class that permits a label to be clicked.
///
/// With this class we can determine de mouse position when is pressed
/// and when the mouse left the label.
///
class ClickableLabel : public QLabel{
    Q_OBJECT
public:

    /// \fn ClickableLabel::ClickableLabel(QWidget *parent)
    /// \~English
    /// \brief Constructor
    /// \~Spanish
    /// \brief Constructor
    explicit ClickableLabel(QWidget *parent = 0);

    /// \fn void ClickableLabel::mouseMoveEvent(QMouseEvent *ev)
    /// \~English
    /// \brief Funtion to add mouse event to the labelWidget
    /// \~Spanish
    /// \brief Funcion para anadir evento del raton (mouse) al labelWidget
    void mouseMoveEvent(QMouseEvent *ev);

    /// \fn void ClickableLabel::mousePressEvent(QMouseEvent *ev)
    /// \~English
    /// \brief Funtion to add mouse event to the labelWidget
    /// \~Spanish
    /// \brief Funcion para anadir evento del raton (mouse) al labelWidget
    void mousePressEvent(QMouseEvent *ev);

    /// \fn void ClickableLabel::leaveEvent(QEvent *)
    /// \~English
    /// \brief Funtion to add mouse event to the labelWidget
    /// \~Spanish
    /// \brief Funcion para anadir evento del raton (mouse) al labelWidget
    void leaveEvent(QEvent *);
    int x,y;

signals:
    void Mouse_Pressed();
    void Mouse_Pos();


    void Mouse_Left();
};

#endif // CLICKABLELABEL_H
