#-------------------------------------------------
#
# Project created by QtCreator 2014-04-06T14:26:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GreenScreen
TEMPLATE = app


SOURCES += main.cpp\
    Filter.cpp \
    MergePanel.cpp \
    ClickableLabel.cpp

HEADERS  += \
    MergePanel.h \
    ClickableLabel.h

FORMS    += \
    MergePanel.ui

RESOURCES += \
    images.qrc \
    style.qrc
