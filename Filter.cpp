// RAAN - 2014-07-19 : Ghost function and minor changes to parameters
/// \file
///
#include "MergePanel.h"

/// \fn void MainWindow::MergeImages(const QImage &objectImage, const QImage &backgroundImage, QImage & mergedImage, int threshold, bool ghost, int x, int y, int offset_x, int offset_y)
/// \~English
/// \brief Funcion that merges two images together. It takes an image with any background color
/// and puts it in another image that will act as a background. (Implements Greenscreen techonology)
/// \~Spanish
/// \brief Funcion que fusiona dos imagenes juntas.  Toma una image con cualquier color de trasfondo
/// y lo pone en otra imagen que actual como el  trasfondo. (Implementa la tecnologia de trasfondo verde)
void MainWindow::MergeImages(const QImage &objectImage, const QImage &backgroundImage, QImage & mergedImage, int threshold, bool ghost, int x, int y,
                 int offset_x, int offset_y){
    

    // YOUR CODE HERE


}
